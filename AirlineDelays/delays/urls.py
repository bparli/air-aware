from django.conf.urls.defaults import patterns, include, url

# Airline Delays patterns.
urlpatterns = patterns('delays.views',
                       (r'^$','home'),
                       (r'^lookups/airlineairport-lookup/','airLookup'),
                       (r'^lookups/route-lookup/','routeLookup'),
                       (r'^lookups/flight-lookup/','flightLookup'),
                       (r'^airlines/airlines-summary/','airlineSummary'),
                       (r'^airports/airports-summary/','airportSummary'),
                       (r'^airlines/best-performing-airlines/','bestAirlines'),
                       (r'^airlines/worst-performing-airlines/','worstAirlines'),
                       (r'^airports/best-performing-airports/','bestAirports'),
                       (r'^airports/worst-performing-airports/','worstAirports'),
                       (r'^airports/compare-airports/','compareAirports'),
                       (r'^airlines/compare-airlines/','compareAirlines'),
                       (r'^airlines/(?P<airline>\w{2})/','airlineLookup'),
                       (r'^airports/(?P<airport>\d{5})/','airportLookup'),
                       (r'^airlines/airlines-3-month-trends/','trends'),
                       (r'^airlines/$','compareAirlines'),
                       (r'^airports/$','compareAirports'),
                       (r'^lookups/','lookups'),
                       (r'^about/$','about'),
                       )
                       
