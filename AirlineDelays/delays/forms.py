from django import forms
from .models import airline_ref_table, airport_ref_table, airline_delays, flightsRollups
from django.db import connection, transaction

class AirlineDelayForm(forms.Form):
    airline = forms.CharField(initial='AA', required = True)
    #airport = forms.CharField(initial = 'SFO')

class AirportDelayForm(forms.Form):
    airport = forms.CharField(initial = 'SFO', max_length = 3, required = True)


class AirDelaysForm(forms.Form):
    airport = forms.ModelChoiceField(queryset=airport_ref_table.objects.all(),required=False)
    airline = forms.ModelChoiceField(queryset=airline_ref_table.objects.all(),required=False)

class CompareAirlinesForm(forms.Form):
    airline1 = forms.ModelChoiceField(queryset=airline_ref_table.objects.all())
    airline2 = forms.ModelChoiceField(queryset=airline_ref_table.objects.all())

class CompareAirportsForm(forms.Form):
    airport1 = forms.ModelChoiceField(queryset=airport_ref_table.objects.all())
    airport2 = forms.ModelChoiceField(queryset=airport_ref_table.objects.all())


class AirportsRouteForm(forms.Form):
    origin_airport = forms.ModelChoiceField(queryset=airport_ref_table.objects.all(),required=True)
    destination_airport = forms.ModelChoiceField(queryset=airport_ref_table.objects.all(),required=True)

    def clean(request):
        cleaned_data = super(AirportsRouteForm, request).clean()
        origin_airport = cleaned_data.get("origin_airport")
        destination_airport = cleaned_data.get("destination_airport")
    
        if origin_airport and destination_airport:
            cursor = connection.cursor()
            cursor.execute('''select unique_carrier from delays_airlinesroutesummary where origin_airport = %s and dest_airport = %s
                                and unique_carrier != 'CO'group by unique_carrier''',[origin_airport.unique_airport_id,destination_airport.unique_airport_id])
            f = cursor.fetchall()
            cursor.close()
            if len(f) == 0:
                raise forms.ValidationError("That direct route does not exist, please select another")
        return cleaned_data

class FlightLookupForm(forms.Form):
    airline = forms.ModelChoiceField(queryset=airline_ref_table.objects.all(),required=True)
    flight_number = forms.IntegerField(required=True, max_value = 10000)

    def clean(self):
        cleaned_data = super(FlightLookupForm, self).clean()
        airline = cleaned_data.get("airline")
        flight_number = cleaned_data.get("flight_number")
    
        if airline and flight_number:
            cursor = connection.cursor()
            check = flightsRollups.objects.filter(unique_carrier = airline.unique_carrier_code, flight = flight_number)[:1]
            if len(check) == 0:
                raise forms.ValidationError("That Airline flight does not exist, please select another")
        return cleaned_data
