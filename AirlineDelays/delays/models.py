from django.db import models
from django.forms import ModelForm
#from django.db import connection, transaction
#from mezzanine.pages.models import Page
#from mezzanine.core.models import Displayable
# Create your models here.

class airline_ref_table(models.Model):
    unique_carrier_code = models.CharField(primary_key = True, unique = True, max_length=11)
    airline_description = models.CharField(max_length=100)
    airline_id = models.IntegerField()

    def __unicode__(self):
        return '%s - %s' %(self.unique_carrier_code, self.airline_description)


class airport_ref_table(models.Model):    
    unique_airport_id = models.IntegerField(primary_key = True, unique = True)
    airport_code = models.CharField(max_length=3)
    airport_sequence = models.IntegerField()
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=3)
    airport_name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s - %s'%(self.airport_code, self.airport_name)

class airline_summary(models.Model):
    airline = models.CharField(max_length = 3)
    #airline = models.ForeignKey('airline_ref_table', related_name='+')
    airline_delays = models.IntegerField()
    airline_cancellations = models.IntegerField()
    avg_delay_length = models.IntegerField()
    num_flights = models.IntegerField()
    airline_pscore = models.FloatField()
    last_updated = models.DateField(auto_now=True)

    def __unicode__(self):
        return self.airline

class CompareAirlinesForm(ModelForm):
    class Meta:
        model = airline_ref_table    

class CompareAirportsForm(ModelForm):
    class Meta:
        model = airport_ref_table   

class airport_summary(models.Model):
    airport = models.CharField(max_length = 3)
    #airport = models.ForeignKey('airport_ref_table', related_name='+')
    airport_id = models.CharField(max_length =6)
    airport_delays = models.IntegerField()
    airport_cancellations = models.IntegerField()
    avg_delay_length = models.IntegerField()
    num_flights = models.IntegerField()
    airport_pscore = models.FloatField()
    last_updated = models.DateField(auto_now=True)

class airportRollups(models.Model):
    unique_airport_id = models.IntegerField()
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    day_of_month = models.IntegerField(max_length = 2)
    day_of_week = models.IntegerField(max_length = 1)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)

class airlineRollups(models.Model):
    unique_carrier_code = models.CharField(max_length=3)
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    day_of_month = models.IntegerField(max_length = 2)
    day_of_week = models.IntegerField(max_length = 1)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)

class airlineMonthRollups(models.Model):
    unique_carrier_code = models.CharField(max_length=3)
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)
    monthdate = models.DateField()
    

class airportMonthRollups(models.Model):
    unique_airport_id = models.IntegerField()
    airport_code = models.CharField(max_length=10)
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)
    monthdate = models.DateField()

class airlineSummaryRollups(models.Model):
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    day_of_month = models.IntegerField(max_length = 2)
    day_of_week = models.IntegerField(max_length = 1)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)

class airportSummaryRollups(models.Model):
    year = models.IntegerField(max_length = 4)
    month = models.IntegerField(max_length = 2)
    day_of_month = models.IntegerField(max_length = 2)
    day_of_week = models.IntegerField(max_length = 1)
    cancellations = models.IntegerField(max_length = 4)
    delays = models.IntegerField(max_length = 5)
    num_flights = models.IntegerField(max_length = 5)

class airportsRouteSummary(models.Model):
    origin_airport = models.IntegerField()
    dest_airport = models.IntegerField()
    num_flights = models.IntegerField()
    delays = models.IntegerField()
    avg_delay_time = models.IntegerField()
    cancellations = models.IntegerField()

class airlinesRouteSummary(models.Model):
    origin_airport = models.IntegerField()
    dest_airport = models.IntegerField()
    unique_carrier = unique_carrier = models.CharField(max_length = 2)
    num_flights = models.IntegerField()
    delays = models.IntegerField()
    avg_delay_time = models.IntegerField()
    cancellations = models.IntegerField()

class routesRollups(models.Model):
    month = models.IntegerField()
    year = models.IntegerField()
    origin_airport = models.IntegerField()
    dest_airport = models.IntegerField()
    num_flights = models.IntegerField(default = 0)
    delays = models.IntegerField(default = 0)
    avg_delay_time = models.IntegerField(default = 0)
    cancellations = models.IntegerField(default = 0)

class flightsRollups(models.Model):
    flight = models.IntegerField(max_length = 4)
    unique_carrier = models.CharField(max_length = 11)
    num_flights = models.IntegerField(default = 0)
    delays = models.IntegerField(default = 0)
    avg_delay_time = models.IntegerField(default = 0)
    cancellations = models.IntegerField(default = 0)

class airlineInfobox(models.Model):
    airline = models.CharField(max_length = 11)
    headquarters = models.CharField(max_length=50)
    frequent_flyer = models.CharField(max_length=50)
    airport_lounge = models.CharField(max_length=50)
    website = models.CharField(max_length=50)
    hubs = models.CommaSeparatedIntegerField(max_length=100)

class airportInfobox(models.Model):
    airport_code = models.CharField(max_length=3)
    airport_name = models.CharField(max_length = 80)
    city = models.CharField(max_length = 30)
    state = models.CharField(max_length = 3)
    latitude = models.IntegerField()
    longitude = models.IntegerField()
    elevation = models.IntegerField()
    time_zone = models.IntegerField()

class airlineTrends(models.Model):
    airline = models.CharField(max_length = 3)
    airline_delays = models.IntegerField()
    airline_drate = models.IntegerField(default = 0)
    airline_cancellations = models.IntegerField()
    airline_crate = models.IntegerField(default = 0)
    avg_delay_length = models.IntegerField()
    num_flights = models.IntegerField()
    pscore = models.FloatField()
    bubble = models.CommaSeparatedIntegerField(max_length=100)

class homeCharts(models.Model):
    delay_cause = models.CharField(max_length = 30)
    delay_rate = models.FloatField()
    cancellation_cause = models.CharField(max_length = 30)
    cancellation_rate = models.FloatField()

class airline_delays(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()
    day_of_week = models.IntegerField()
    day_of_month = models.IntegerField()
    fl_date = models.DateField()
    #unique_carrier = models.ForeignKey('airline_ref_table')
    unique_carrier = models.CharField(max_length = 11)
    #origin_airport = models.ForeignKey('airport_ref_table', related_name = 'origin_airport_id')
    origin_airport = models.IntegerField()
    #dest_airport = models.ForeignKey('airport_ref_table', related_name = 'dest_airport_id')
    dest_airport = models.IntegerField()
    flight_number = models.IntegerField(max_length = 4)
    dep_time = models.IntegerField(max_length = 4)
    dep_delay = models.IntegerField(max_length = 5)
    taxi_out = models.IntegerField(max_length = 4)
    taxi_in = models.IntegerField(max_length = 4)
    arrival_delay = models.IntegerField(max_length = 5)
    canceled = models.IntegerField(max_length = 1)
    cancellation_code = models.CharField(max_length = 1)
    carrier_delay = models.IntegerField(max_length = 5)
    weather_delay = models.IntegerField(max_length = 5)
    nas_delay = models.IntegerField(max_length = 5)
    security_delay = models.IntegerField(max_length = 5)
    late_aircraft_delay = models.IntegerField(max_length = 5)

