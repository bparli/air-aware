#from django.shortcuts import render
from .forms import AirDelaysForm, CompareAirlinesForm, CompareAirportsForm, AirportsRouteForm, FlightLookupForm
from .models import homeCharts, airlineInfobox, airportInfobox, airlineTrends, routesRollups, flightsRollups, airportsRouteSummary, airlinesRouteSummary, airline_summary, airport_summary, airline_ref_table, airport_ref_table, airline_delays, airportMonthRollups, airlineSummaryRollups,airportSummaryRollups, airlineMonthRollups
from django.db import connection, transaction
from chartit import DataPool, Chart, PivotDataPool, PivotChart
from django.db.models import Avg, Sum, Count, Q
from datetime import *
from django.shortcuts import redirect
import urllib2, json
from django.core.cache import cache
from mezzanine.utils.views import render
from mezzanine.conf import settings

start_date = 'January 1, 2010'
end_date = 'March 31, 2013'

def get_device(request):
    try:
        if request.COOKIES["mezzanine-device"]:
            return request.COOKIES["mezzanine-device"]
    except KeyError:
        try:
            user_agent = request.META["HTTP_USER_AGENT"].lower()
        except KeyError:
            pass
        else:
            for (device, ua_strings) in settings.DEVICE_USER_AGENTS:
                for ua_string in ua_strings:
                    if ua_string.lower() in user_agent:
                        return device
    return ""

def about(request):
    context = {"start_date":start_date,"end_date":end_date}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/About.html', context)
    else:
        return render(request, 'delays/About.html', context)

def terms(request):
    context = {}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Terms.html', context)
    else:
        return render(request, 'delays/Terms.html', context)
    

def home(request):

    if request.method == 'POST': # If the form has been submitted...
        if 'air_lookup' in request.POST:
            form = AirDelaysForm(request.POST) # A form bound to the POST data
            #Airport only
            if airline == '' and airport != '':
                return redirect('/airports/%s' % airport)
            #Airline only
            elif airport == '' and airline != '':
                return redirect('/airlines/%s' % airline)
            #Search combination
            elif airport != '' and airline != '':
                return airLookup(request)        
        elif 'routes_lookup' in request.POST:
            form = AirportsRouteForm(request.POST)
            return routeLookup(request)
        elif 'flights_lookup' in request.POST:
            form = FlightLookupForm(request.POST)
            return flightLookup(request)

    else:
        Form2 = AirportsRouteForm()
        Form1 = AirDelaysForm()
        Form3 = FlightLookupForm()
    
        #context = {"Form3":Form3,"Form2":Form2,"Form1":Form1}
        #return render(request, 'delays/lookups.html', context)

    airdata1 = \
        DataPool(
           series=
            [{'options': {
               'source': homeCharts.objects.filter(delay_cause__in=['Carrier','NAS','Weather','Security','Late Aircraft'])},
              'terms': [
                'delay_cause',
                'delay_rate']}
             ])

    airchart1 = Chart(
            datasource = airdata1,
            series_options =
              [{'options':{
                  'type': 'column',
                  'stacking': False},
                'terms':{
                  'delay_cause': [
                    'delay_rate']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Causes of Flight Delays'},
               'xAxis': {
                    'title': {
                       'text': 'Flight Delays Causes'}},
               'credits': {'text': ''},
               'yAxis': {
                    'title': {
                       'text': 'Percent Total Delays'}}})

    airdata2 = \
        DataPool(
           series=
            [{'options': {
               'source': homeCharts.objects.filter(cancellation_cause__in=['Carrier','NAS','Weather','Security'])},
              'terms': [
                'cancellation_cause',
                'cancellation_rate']}
             ])

    airchart2 = Chart(
            datasource = airdata2,
            series_options =
              [{'options':{
                  'type': 'column',
                  'stacking': False},
                'terms':{
                  'cancellation_cause': [
                    'cancellation_rate']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Causes of Flight Cancellations'},
               'xAxis': {
                    'title': {
                       'text': 'Flight Cancellations Causes'}},
               'credits': {'text': ''},
               'yAxis': {
                    'title': {
                       'text': 'Percent Total Cancellations'}}})

    title = "Home"
    cht_list=[airchart1,airchart2] 
    context = {"title":title,"cht_list":cht_list,"Form3":Form3,"Form2":Form2,"Form1":Form1}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Home.html', context)
    else:
        return render(request, 'delays/Home.html', context) 

def monthname(month_num):
    names ={1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun',
            7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec'}
    #month_num = int(month_num[0][0])
    if month_num not in names:
        return month_num
    else:
        return (names[month_num], )

def airlines_avg():
    cursor = connection.cursor()
    #average delays rate per airline 
    cursor.execute('''select sum(airline_delays)/sum(num_flights)
                       from delays_airline_summary''')
    avgDelayRate = cursor.fetchone()
    avgDelayRate = str(avgDelayRate[0]*100)[0:4]
    
    #avg airline delay length
    cursor.execute('''select avg(avg_delay_length)
                        from delays_airline_summary''')
    avgDelayTime = cursor.fetchone()
    avgDelayTime = int(avgDelayTime[0])
    
    #average airline cancellations rate in a month
    cursor.execute('''select sum(airline_cancellations)/sum(num_flights)
                       from delays_airline_summary''')
    avgCanRate = cursor.fetchone()
    avgCanRate = str(avgCanRate[0]*100)[0:4]

    #avg airline P-Score
    cursor.execute('''select avg(airline_pscore) from delays_airline_summary''')
    avgPscore = str(cursor.fetchone()[0])[0:4]

    cursor.close()
    
    avg = [avgDelayRate,avgDelayTime,avgCanRate,avgPscore]
    return avg

def airlineRates(airline):
    cursor = connection.cursor()
    #delays rate per airport       
    cursor.execute('''select (airline_delays/num_flights)
                      from delays_airline_summary where airline = %s''', [airline])
    airlineDelayRate = cursor.fetchone()
    airlineDelayRate = str(airlineDelayRate[0]*100)[0:4]

    #average delay time
    air1 = airline_summary.objects.get(airline = airline)
    avgDelayTime = air1.avg_delay_length

    #airline cancellations rate
    cursor.execute('''select (airline_cancellations/num_flights)
                    from delays_airline_summary where airline = %s''', [airline])
    airlineCanRate = cursor.fetchone()
    airlineCanRate = str(airlineCanRate[0]*100)[0:4]

    #airline Pscore
    airlinePscore  = air1.airline_pscore
    
    cursor.close()

    return [airlineDelayRate,avgDelayTime,airlineCanRate, airlinePscore]

def airportRates(airport):
    cursor = connection.cursor()        
    
    #delays rate per airport 
    cursor.execute('''select (airport_delays/num_flights)
                     from delays_airport_summary where airport_id = %s''', [airport])
    airportDelayRate = cursor.fetchone()
    airportDelayRate = str(airportDelayRate[0]*100)[0:4]
         
    #avg airport delay length
    airp = airport_summary.objects.get(airport_id = airport)
    avgDelayTime = airp.avg_delay_length
          
    #airport cancellations rate
    cursor.execute('''select (airport_cancellations/num_flights)
                     from delays_airport_summary where airport_id = %s''', [airport])
    airportCanRate = cursor.fetchone()
    airportCanRate = str(airportCanRate[0]*100)[0:4]

    #Airport Pscore
    airportPscore = airp.airport_pscore

    cursor.close()

    return [airportDelayRate,avgDelayTime,airportCanRate, airportPscore]    

def airports_avg():
    cursor = connection.cursor()
    #avg delays rate per airport
    cursor.execute('''select sum(airport_delays)/sum(num_flights)
                       from delays_airport_summary where num_flights > 500''')
    avgDelayRate = cursor.fetchone()
    avgDelayRate = str(avgDelayRate[0]*100)[0:4]
    
    #avg airport delay length
    cursor.execute('''select avg(avg_delay_length)
                        from delays_airport_summary where num_flights > 500''')
    avgDelayTime = cursor.fetchone()
    avgDelayTime = int(avgDelayTime[0])
    
    #avg airport cancellations rate per airport
    cursor.execute('''select sum(airport_cancellations)/sum(num_flights)
                       from delays_airport_summary where num_flights > 500''')
    avgCanRate = cursor.fetchone()
    avgCanRate = str(avgCanRate[0]*100)[0:4]

    #avg airport P-Score
    cursor.execute('''select avg(airport_pscore) from delays_airport_summary''')
    avgPscore = str(cursor.fetchone()[0])[0:4]
    
    cursor.close()

    avg = [avgDelayRate,avgDelayTime,avgCanRate,avgPscore]
    return avg

def airlineSummary(request): 
    avg = airlines_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgPscore = avg[3]

    airdata1 = \
    DataPool(
       series=
        [{'options': {
            'source': airlineMonthRollups.objects.filter(year=2012).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2012 month':'month'},
            {'2012 Delays':'delays'}]},
         {'options': {
            'source': airlineMonthRollups.objects.filter(year=2011).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2011 month':'month'},
            {'2011 Delays':'delays'}]},
         {'options': {
            'source': airlineMonthRollups.objects.filter(year=2010).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2010 month':'month'},
            {'2010 Delays':'delays'}]},
        {'options': {
           'source': airlineMonthRollups.objects.filter(year=2013).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2013 month':'month'},
            {'2013 Delays':'delays'}]}])
       
    airchart1 = Chart(
        datasource = airdata1,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{
                '2010 month': [
                '2010 Delays'],
              '2011 month': [
                '2011 Delays'],
              '2012 month': [
                '2012 Delays'],
              '2013 month': [
                '2013 Delays']
              }}],
        chart_options =
          {'title': {
               'text': 'Monthly Airline Delays Trends'},
           'xAxis': {
                'title': {
                   'text': 'Months'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Airline Delays'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    airdata2 = \
        DataPool(
       series=
        [{'options': {
           'source': airlineMonthRollups.objects.filter(year=2012).values('month').annotate(delays=Sum('cancellations'))},
          'terms': [
            {'2012 month':'month'},
            {'2012 Cancellations':'cancellations'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(year=2011).values('month').annotate(delays=Sum('cancellations'))},
          'terms': [
            {'2011 month':'month'},
            {'2011 Cancellations':'cancellations'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(year=2010).values('month').annotate(delays=Sum('cancellations'))},
          'terms': [
            {'2010 month':'month'},
            {'2010 Cancellations':'cancellations'}]},
        {'options': {
           'source': airlineMonthRollups.objects.filter(year=2013).values('month').annotate(delays=Sum('cancellations'))},
          'terms': [
            {'2013 month':'month'},
            {'2013 Cancellations':'cancellations'}]}])

    airchart2 = Chart(
        datasource = airdata2,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{
              '2012 month': [
                '2012 Cancellations'],
              '2011 month': [
                '2011 Cancellations'],
              '2010 month': [
                '2010 Cancellations'],
              '2013 month': [
                '2013 Cancellations']
              }}],
        chart_options =
          {'title': {
               'text': 'Monthly Airline Cancellations Trends'},
           'xAxis': {
                'title': {
                   'text': 'Months'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Airline Cancellations'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))
    
    cht_list=[airchart1,airchart2]
    statement = """* U.S. Airline Performance Scores (P-Scores) are determined by calculating the rate of delays, average length of delay,
		and rate of cancellations attributed to a given airline.  The impact of a cancellation is also given more weight (equivalent to 
		a 12 hour delay time) as compared to a delay."""
    title = "Airlines Average On-Time Performance"
    title2 = "Airline Delays Monthly Trends From %s to %s" % (start_date,end_date)
    title3 = "Airline Cancellations Monthly Trends From %s to %s" % (start_date,end_date)
    context = {"title":title,"avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgPscore":avgPscore,
               'cht_list': cht_list,"title3":title3, "title2":title2,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Summary.html', context)
    else:
        return render(request, 'delays/Summary.html', context) 

def airportSummary(request): 
    avg = airports_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgPscore = avg[3]
    
    airdata1 = \
    DataPool(
       series=
        [{'options': {
           'source': airportMonthRollups.objects.filter(year=2012).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2012 month':'month'},
            {'2012 Delays':'delays'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(year=2011).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2011 month':'month'},
            {'2011 Delays':'delays'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(year=2010).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2010 month':'month'},
            {'2010 Delays':'delays'}]},
        {'options': {
           'source': airportMonthRollups.objects.filter(year=2013).values('month').annotate(delays=Sum('delays'))},
          'terms': [
            {'2013 month':'month'},
            {'2013 Delays':'delays'}]}])

    airchart1 = Chart(
        datasource = airdata1,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{
              '2012 month': [
                '2012 Delays'],
              '2011 month': [
                '2011 Delays'],
              '2010 month': [
                '2010 Delays'],
              '2013 month': [
                '2013 Delays']
              }}],
        chart_options =
          {'title': {
               'text': 'Monthly Airport Delays Trends'},
           'xAxis': {
                'title': {
                   'text': 'Months'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Airport Delays'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    airdata2 = \
        DataPool(
       series=
        [{'options': {
           'source': airportMonthRollups.objects.filter(year=2012).values('month').annotate(cancellations=Sum('cancellations'))},
          'terms': [
            {'2012 month':'month'},
            {'2012 Cancellations':'cancellations'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(year=2011).values('month').annotate(cancellations=Sum('cancellations'))},
          'terms': [
            {'2011 month':'month'},
            {'2011 Cancellations':'cancellations'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(year=2010).values('month').annotate(cancellations=Sum('cancellations'))},
          'terms': [
            {'2010 month':'month'},
            {'2010 Cancellations':'cancellations'}]},
        {'options': {
           'source': airportMonthRollups.objects.filter(year=2013).values('month').annotate(cancellations=Sum('cancellations'))},
          'terms': [
            {'2013 month':'month'},
            {'2013 Cancellations':'cancellations'}]}])

    airchart2 = Chart(
        datasource = airdata2,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{
              '2012 month': [
                '2012 Cancellations'],
              '2011 month': [
                '2011 Cancellations'],
              '2010 month': [
                '2010 Cancellations'],
              '2013 month': [
                '2013 Cancellations']
              }}],
        chart_options =
          {'title': {
               'text': 'Monthly Airport Cancellations Trends'},
           'xAxis': {
                'title': {
                   'text': 'Months'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Airport Cancellations'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    statement = """*U.S. Airport Performance Scores (P-Scores) are determined by calculating the rate of delays,
                average length of delay, and rate of cancellations attributed to a given airport. The impact of a cancellation
                is also given more weight (equivalent to a 12 hour delay time) as compared to a delay. This way U.S.
                Airline performance can be compared or ranked."""    
    cht_list=[airchart1,airchart2]
    title = "Airports Average On-Time Performance"
    title2 = "Airport Delays Monthly Trends From %s to %s" % (start_date,end_date)
    title3 = "Airport Cancellations Monthly Trends From %s to %s" % (start_date,end_date)
    context = {"title":title,"avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgPscore":avgPscore,
               "cht_list":cht_list,"title2":title2,"title3":title3,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Summary.html', context)
    else:
        return render(request, 'delays/Summary.html', context) 

def bestAirports(request):
    airports = airport_summary.objects.all().order_by('-num_flights')[:75]
    tmp = []
    for e in airports:
        tmp.append((e.airport,e.airport_pscore,(float(e.airport_delays)/float(e.num_flights)),e.avg_delay_length,float(e.airport_cancellations)/float(e.num_flights),e.airport_id))

    sort = sorted(tmp,key= lambda x: x[1])
    airport1=sort[0][0]
    airport2=sort[1][0]
    airport3=sort[2][0]
    airport4=sort[3][0]
    airport5=sort[4][0]
    airport6=sort[5][0]
    airport7=sort[6][0]
    airport8=sort[7][0]
    airport9=sort[8][0]
    airport10=sort[9][0]
    a1 = airport_ref_table.objects.get(airport_code = airport1)
    a2 = airport_ref_table.objects.get(airport_code = airport2)
    a3 = airport_ref_table.objects.get(airport_code = airport3)
    a4 = airport_ref_table.objects.get(airport_code = airport4)
    a5 = airport_ref_table.objects.get(airport_code = airport5)
    a6 = airport_ref_table.objects.get(airport_code = airport6)
    a7 = airport_ref_table.objects.get(airport_code = airport7)
    a8 = airport_ref_table.objects.get(airport_code = airport8)
    a9 = airport_ref_table.objects.get(airport_code = airport9)
    a10 = airport_ref_table.objects.get(airport_code = airport10)
    a1delays=str(sort[0][2]*100)[:4]
    a2delays=str(sort[1][2]*100)[:4]
    a3delays=str(sort[2][2]*100)[:4]
    a4delays=str(sort[3][2]*100)[:4]
    a5delays=str(sort[4][2]*100)[:4]
    a6delays=str(sort[5][2]*100)[:4]
    a7delays=str(sort[6][2]*100)[:4]
    a8delays=str(sort[7][2]*100)[:4]
    a9delays=str(sort[8][2]*100)[:4]
    a10delays=str(sort[9][2]*100)[:4]
    a1delaylength=sort[0][3]
    a2delaylength=sort[1][3]
    a3delaylength=sort[2][3]
    a4delaylength=sort[3][3]
    a5delaylength=sort[4][3]
    a6delaylength=sort[5][3]
    a7delaylength=sort[6][3]
    a8delaylength=sort[7][3]
    a9delaylength=sort[8][3]
    a10delaylength=sort[9][3]
    a1cans=str(sort[0][4]*100)[0:5]
    a2cans=str(sort[1][4]*100)[0:5]
    a3cans=str(sort[2][4]*100)[0:5]
    a4cans=str(sort[3][4]*100)[0:5]
    a5cans=str(sort[4][4]*100)[0:5]
    a6cans=str(sort[5][4]*100)[0:5]
    a7cans=str(sort[6][4]*100)[0:5]
    a8cans=str(sort[7][4]*100)[0:5]
    a9cans=str(sort[8][4]*100)[0:5]
    a10cans=str(sort[9][4]*100)[0:5]
    a1p = str(sort[0][1])[0:4]
    a2p = str(sort[1][1])[0:4]
    a3p = str(sort[2][1])[0:4]
    a4p = str(sort[3][1])[0:4]
    a5p = str(sort[4][1])[0:4]
    a6p = str(sort[5][1])[0:4]
    a7p = str(sort[6][1])[0:4]
    a8p = str(sort[7][1])[0:4]
    a9p = str(sort[8][1])[0:4]
    a10p = str(sort[9][1])[0:4]
    link1 = "/airports/%s" % sort[0][5]
    link2 = "/airports/%s" % sort[1][5]
    link3 = "/airports/%s" % sort[2][5]
    link4 = "/airports/%s" % sort[3][5]
    link5 = "/airports/%s" % sort[4][5]
    link6 = "/airports/%s" % sort[5][5]
    link7 = "/airports/%s" % sort[6][5]
    link8 = "/airports/%s" % sort[7][5]
    link9 = "/airports/%s" % sort[8][5]
    link10 = "/airports/%s" % sort[9][5]
    
    avg = airports_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgp = avg[3]

    airdata = \
        DataPool(
           series=
            [{'options': {
               'source': airport_summary.objects.filter(airport__in=[airport1, airport2, airport3, airport4,
                                                                     airport5, sort[5][0],sort[6][0], sort[7][0],
                                                                     sort[8][0], sort[9][0]])},
              'terms': [
                'airport',
                'airport_pscore']}
             ])

    airchart = Chart(
            datasource = airdata,
            series_options =
              [{'options':{
                  'type': 'bar',
                  'stacking': False},
                'terms':{
                  'airport': [
                    'airport_pscore']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Best Airports Rankings'},
               'xAxis': {
                    'title': {
                       'text': 'Best Airports'}},
               'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
               'yAxis': {
                    'title': {
                       'text': 'P-Scores'},
                    'max': avgp,
                     'plotLines': [{
                        'color': '#FF0000',
                        'width': 4,
                        'value': avgp,
                        'zIndex':5,
                        'label':{
                            'align':'right',
                            'x':-10,
                            'rotation':0,
                            'text':'Average P-Score',
                            'style': {
                                'color': 'blue',
                                'fontWeight': 'bold'}}
               }]}})
    Category = "Airports"
    statement = """*U.S. Airport Performance Scores (P-Scores) are determined by calculating the rate of delays,
                average length of delay, and rate of cancellations attributed to a given airport. The impact of a cancellation
                is also given more weight (equivalent to a 12 hour delay time) as compared to a delay. This way U.S.
                Airline performance can be compared or ranked."""    
    title = "On-Time Performance Rankings - Top Airports"
    title2 = "On-Time Performance Summary"
    title3 = "P-Scores Comparison - Top Airports"
    context = {"title":title,"title2":title2,"title3":title3,"Category":Category,"a1":a1,"link1":link1,"a1delays":a1delays,"a1delaylength":a1delaylength,
               "a1cans":a1cans,"a1p":a1p,"a2":a2,"link2":link2,"a2delays":a2delays,"a2delaylength":a2delaylength,"a2cans":a2cans,"a2p":a2p,
               "a3":a3,"link3":link3,"a3delays":a3delays,"a3delaylength":a3delaylength,"a3cans":a3cans,"a3p":a3p,
               "a4":a4,"link4":link4,"a4delays":a4delays,"a4delaylength":a4delaylength,"a4cans":a4cans,"a4p":a4p,
               "a5":a5,"link5":link5,"a5delays":a5delays,"a5delaylength":a5delaylength,"a5cans":a5cans,"a5p":a5p,
                "a6":a6,"link6":link6,"a6delays":a6delays,"a6delaylength":a6delaylength,"a6cans":a5cans,"a6p":a6p,
                "a7":a7,"link7":link7,"a7delays":a7delays,"a7delaylength":a7delaylength,"a7cans":a7cans,"a7p":a7p,
                "a8":a8,"link8":link8,"a8delays":a8delays,"a8delaylength":a8delaylength,"a8cans":a8cans,"a8p":a8p,
                "a9":a9,"link9":link9,"a9delays":a9delays,"a9delaylength":a9delaylength,"a9cans":a9cans,"a9p":a9p,
                "a10":a10,"link10":link10,"a10delays":a10delays,"a10delaylength":a10delaylength,"a10cans":a10cans,"a10p":a10p,
               "avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgp":avgp,
               "airchart":airchart,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Best_Worst_Airports.html', context)
    else:
        return render(request, 'delays/Best_Worst_Airports.html', context) 

def worstAirports(request):
    airports = airport_summary.objects.all().order_by('-num_flights')[:75]
    tmp = []
    for e in airports:
        tmp.append((e.airport,e.airport_pscore,(float(e.airport_delays)/float(e.num_flights)),e.avg_delay_length,float(e.airport_cancellations)/float(e.num_flights),e.airport_id))

    sort = sorted(tmp,key= lambda x: x[1])
    airport1=sort[-1][0]
    airport2=sort[-2][0]
    airport3=sort[-3][0]
    airport4=sort[-4][0]
    airport5=sort[-5][0]
    airport6=sort[-6][0]
    airport7=sort[-7][0]
    airport8=sort[-8][0]
    airport9=sort[-9][0]
    airport10=sort[-10][0]
    a1 = airport_ref_table.objects.get(airport_code = airport1)
    a2 = airport_ref_table.objects.get(airport_code = airport2)
    a3 = airport_ref_table.objects.get(airport_code = airport3)
    a4 = airport_ref_table.objects.get(airport_code = airport4)
    a5 = airport_ref_table.objects.get(airport_code = airport5)
    a6 = airport_ref_table.objects.get(airport_code = airport6)
    a7 = airport_ref_table.objects.get(airport_code = airport7)
    a8 = airport_ref_table.objects.get(airport_code = airport8)
    a9 = airport_ref_table.objects.get(airport_code = airport9)
    a10 = airport_ref_table.objects.get(airport_code = airport10)
    a1delays=str(sort[-1][2]*100)[:4]
    a2delays=str(sort[-2][2]*100)[:4]
    a3delays=str(sort[-3][2]*100)[:4]
    a4delays=str(sort[-4][2]*100)[:4]
    a5delays=str(sort[-5][2]*100)[:4]
    a6delays=str(sort[-6][2]*100)[:4]
    a7delays=str(sort[-7][2]*100)[:4]
    a8delays=str(sort[-8][2]*100)[:4]
    a9delays=str(sort[-9][2]*100)[:4]
    a10delays=str(sort[-10][2]*100)[:4]
    a1delaylength=sort[-1][3]
    a2delaylength=sort[-2][3]
    a3delaylength=sort[-3][3]
    a4delaylength=sort[-4][3]
    a5delaylength=sort[-5][3]
    a6delaylength=sort[-6][3]
    a7delaylength=sort[-7][3]
    a8delaylength=sort[-8][3]
    a9delaylength=sort[-9][3]
    a10delaylength=sort[-10][3]
    a1cans=str(sort[-1][4]*100)[:4]
    a2cans=str(sort[-2][4]*100)[:4]
    a3cans=str(sort[-3][4]*100)[:4]
    a4cans=str(sort[-4][4]*100)[:4]
    a5cans=str(sort[-5][4]*100)[:4]
    a6cans=str(sort[-6][4]*100)[:4]
    a7cans=str(sort[-7][4]*100)[:4]
    a8cans=str(sort[-8][4]*100)[:4]
    a9cans=str(sort[-9][4]*100)[:4]
    a10cans=str(sort[-10][4]*100)[:4]
    a1p = str(sort[-1][1])[0:4]
    a2p = str(sort[-2][1])[0:4]
    a3p = str(sort[-3][1])[0:4]
    a4p = str(sort[-4][1])[0:4]
    a5p = str(sort[-5][1])[0:4]
    a6p = str(sort[-6][1])[0:4]
    a7p = str(sort[-7][1])[0:4]
    a8p = str(sort[-8][1])[0:4]
    a9p = str(sort[-9][1])[0:4]
    a10p = str(sort[-10][1])[0:4]
    link1 = "/airports/%s" % sort[-1][5]
    link2 = "/airports/%s" % sort[-2][5]
    link3 = "/airports/%s" % sort[-3][5]
    link4 = "/airports/%s" % sort[-4][5]
    link5 = "/airports/%s" % sort[-5][5]
    link6 = "/airports/%s" % sort[-6][5]
    link7 = "/airports/%s" % sort[-7][5]
    link8 = "/airports/%s" % sort[-8][5]
    link9 = "/airports/%s" % sort[-9][5]
    link10 = "/airports/%s" % sort[-10][5]
    
    avg = airports_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgp = avg[3]

    airdata = \
        DataPool(
           series=
            [{'options': {
               'source': airport_summary.objects.filter(airport__in=[airport1, airport2, airport3, airport4,
                                                                     airport5, sort[-5][0],sort[-6][0], sort[-7][0],
                                                                     sort[-8][0], sort[-9][0]])},
              'terms': [
                'airport',
                'airport_pscore']}
             ])

    airchart = Chart(
            datasource = airdata,
            series_options =
              [{'options':{
                  'type': 'bar',
                  'stacking': False},
                'terms':{
                  'airport': [
                    'airport_pscore']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Worst Airports Rankings'},
               'xAxis': {
                    'title': {
                       'text': 'Worst Airports'}},
               'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
               'yAxis': {
                    'title': {
                       'text': 'P-Scores'},
                     'plotLines': [{
                        'color': '#FF0000',
                        'width': 4,
                        'value': avgp,
                        'zIndex':5,
                        'label':{
                            'align':'right',
                            'x':-10,
                            'y':5,
                            'rotation':0,
                            'text':'Average P-Score',
                            'style': {
                                'color': 'blue',
                                'fontWeight': 'bold'}}
               }]}})
    Category = "Airports"
    statement = """*U.S. Airport Performance Scores (P-Scores) are determined by calculating the rate of delays,
                average length of delay, and rate of cancellations attributed to a given airport. The impact of a cancellation
                is also given more weight (equivalent to a 12 hour delay time) as compared to a delay. This way U.S.
                Airline performance can be compared or ranked."""    
    title = "On-Time Performance Rankings - Worst Airports"
    title2 = "On-Time Performance Summary"
    title3 = "P-Scores Comparison - Worst Airports"
    context = {"title":title,"title2":title2,"title3":title3,"Category":Category,"a1":a1,"link1":link1,"a1delays":a1delays,"a1delaylength":a1delaylength,"a1cans":a1cans,"a1p":a1p,
               "a2":a2,"link2":link2,"a2delays":a2delays,"a2delaylength":a2delaylength,"a2cans":a2cans,"a2p":a2p,
               "a3":a3,"link3":link3,"a3delays":a3delays,"a3delaylength":a3delaylength,"a3cans":a3cans,"a3p":a3p,
               "a4":a4,"link4":link4,"a4delays":a4delays,"a4delaylength":a4delaylength,"a4cans":a4cans,"a4p":a4p,
               "a5":a5,"link5":link5,"a5delays":a5delays,"a5delaylength":a5delaylength,"a5cans":a5cans,"a5p":a5p,
                "a6":a6,"link6":link6,"a6delays":a6delays,"a6delaylength":a6delaylength,"a6cans":a5cans,"a6p":a6p,
                "a7":a7,"link7":link7,"a7delays":a7delays,"a7delaylength":a7delaylength,"a7cans":a7cans,"a7p":a7p,
                "a8":a8,"link8":link8,"a8delays":a8delays,"a8delaylength":a8delaylength,"a8cans":a8cans,"a8p":a8p,
                "a9":a9,"link9":link9,"a9delays":a9delays,"a9delaylength":a9delaylength,"a9cans":a9cans,"a9p":a9p,
                "a10":a10,"link10":link10,"a10delays":a10delays,"a10delaylength":a10delaylength,"a10cans":a10cans,"a10p":a10p,
               "avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgp":avgp,
               'airchart':airchart,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Best_Worst_Airports.html', context)
    else:
        return render(request, 'delays/Best_Worst_Airports.html', context) 
        
def bestAirlines(request):
    airlines = airline_summary.objects.all()
    tmp = []    
    for e in airlines:
        tmp.append((e.airline,e.airline_pscore,(float(e.airline_delays)/float(e.num_flights)),e.avg_delay_length,float(e.airline_cancellations)/float(e.num_flights)))

    sort = sorted(tmp,key= lambda x: x[1])
    airline1=sort[0][0]
    airline2=sort[1][0]
    airline3=sort[2][0]
    airline4=sort[3][0]
    airline5=sort[4][0]
    a1 = airline_ref_table.objects.get(unique_carrier_code = airline1)
    a2 = airline_ref_table.objects.get(unique_carrier_code = airline2)
    a3 = airline_ref_table.objects.get(unique_carrier_code = airline3)
    a4 = airline_ref_table.objects.get(unique_carrier_code = airline4)
    a5 = airline_ref_table.objects.get(unique_carrier_code = airline5)
    a1delays=str(sort[0][2]*100)[:4]
    a2delays=str(sort[1][2]*100)[:4]
    a3delays=str(sort[2][2]*100)[:4]
    a4delays=str(sort[3][2]*100)[:4]
    a5delays=str(sort[4][2]*100)[:4]
    a1delaylength=sort[0][3]
    a2delaylength=sort[1][3]
    a3delaylength=sort[2][3]
    a4delaylength=sort[3][3]
    a5delaylength=sort[4][3]
    a1cans=str(sort[0][4]*100)[:4]
    a2cans=str(sort[1][4]*100)[:4]
    a3cans=str(sort[2][4]*100)[:4]
    a4cans=str(sort[3][4]*100)[:4]
    a5cans=str(sort[4][4]*100)[:4]
    a1p = str(sort[0][1])[0:4]
    a2p = str(sort[1][1])[0:4]
    a3p = str(sort[2][1])[0:4]
    a4p = str(sort[3][1])[0:4]
    a5p = str(sort[4][1])[0:4]
    link1 = "/airlines/%s" % airline1
    link2 = "/airlines/%s" % airline2
    link3 = "/airlines/%s" % airline3
    link4 = "/airlines/%s" % airline4
    link5 = "/airlines/%s" % airline5
    

    avg = airlines_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgp = avg[3]

    airdata = \
        DataPool(
           series=
            [{'options': {
               'source': airline_summary.objects.filter(airline__in=[airline1, airline2, airline3, airline4, airline5])},
              'terms': [
                'airline',
                'airline_pscore']}
             ])

    airchart = Chart(
            datasource = airdata,
            series_options =
              [{'options':{
                  'type': 'bar',
                  'stacking': False},
                'terms':{
                  'airline': [
                    'airline_pscore']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Best Airlines Rankings'},
               'xAxis': {
                    'title': {
                       'text': 'Best Airlines'}},
               'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
               'yAxis': {
                    'title': {
                       'text': 'P-Scores'},
                    'max': avgp,
                     'plotLines': [{
                        'color': '#FF0000',
                        'width': 4,
                        'value': avgp,
                        'zIndex':5,
                        'label':{
                            'align':'right',
                            'x':-10,
                            'rotation':0,
                            'text':'Average P-Score',
                            'style': {
                                'color': 'blue',
                                'fontWeight': 'bold'}}
                        }]}
               })
    Category = "Airlines"
    statement = """* U.S. Airline Performance Scores (P-Scores) are determined by calculating the rate of delays, average length of delay,
		and rate of cancellations attributed to a given airline.  The impact of a cancellation is also given more weight (equivalent to 
		a 12 hour delay time) as compared to a delay."""
    title = "On-Time Performance Rankings - Top Airlines"
    title2 = "On-Time Performance Summary"
    title3 = "P-Scores Comparison - Top Airlines"
    context = {"title":title,"title2":title2,"title3":title3,"Category":Category,"a1":a1,"link1":link1,"a1delays":a1delays,"a1delaylength":a1delaylength,"a1cans":a1cans,"a1p":a1p,
               "a2":a2,"link2":link2,"a2delays":a2delays,"a2delaylength":a2delaylength,"a2cans":a2cans,"a2p":a2p,
               "a3":a3,"link3":link3,"a3delays":a3delays,"a3delaylength":a3delaylength,"a3cans":a3cans,"a3p":a3p,
               "a4":a4,"link4":link4,"a4delays":a4delays,"a4delaylength":a4delaylength,"a4cans":a3cans,"a4p":a4p,
               "a5":a5,"link5":link5,"a5delays":a5delays,"a5delaylength":a5delaylength,"a5cans":a5cans,"a5p":a5p,
                "avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgp":avgp,
               "airchart":airchart,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Best_Worst.html', context)
    else:
        return render(request, 'delays/Best_Worst.html', context) 

def worstAirlines(request):
    airlines = airline_summary.objects.all()
    tmp = []    
    for e in airlines:
        tmp.append((e.airline,e.airline_pscore,(float(e.airline_delays)/float(e.num_flights)),e.avg_delay_length,float(e.airline_cancellations)/float(e.num_flights)))

    sort = sorted(tmp,key= lambda x: x[1])
    airline1=sort[-1][0]
    airline2=sort[-2][0]
    airline3=sort[-3][0]
    airline4=sort[-4][0]
    airline5=sort[-5][0]
    a1 = airline_ref_table.objects.get(unique_carrier_code = airline1)
    a2 = airline_ref_table.objects.get(unique_carrier_code = airline2)
    a3 = airline_ref_table.objects.get(unique_carrier_code = airline3)
    a4 = airline_ref_table.objects.get(unique_carrier_code = airline4)
    a5 = airline_ref_table.objects.get(unique_carrier_code = airline5)
    a1delays=str(sort[-1][2]*100)[:4]
    a2delays=str(sort[-2][2]*100)[:4]
    a3delays=str(sort[-3][2]*100)[:4]
    a4delays=str(sort[-4][2]*100)[:4]
    a5delays=str(sort[-5][2]*100)[:4]
    a1delaylength=sort[-1][3]
    a2delaylength=sort[-1][3]
    a3delaylength=sort[-3][3]
    a4delaylength=sort[-4][3]
    a5delaylength=sort[-5][3]
    a1cans=str(sort[-1][4]*100)[:4]
    a2cans=str(sort[-2][4]*100)[:4]
    a3cans=str(sort[-3][4]*100)[:4]
    a4cans=str(sort[-4][4]*100)[:4]
    a5cans=str(sort[-5][4]*100)[:4]
    a1p = str(sort[-1][1])[0:4]
    a2p = str(sort[-2][1])[0:4]
    a3p = str(sort[-3][1])[0:4]
    a4p = str(sort[-4][1])[0:4]
    a5p = str(sort[-5][1])[0:4]
    link1 = "/airlines/%s" % airline1
    link2 = "/airlines/%s" % airline2
    link3 = "/airlines/%s" % airline3
    link4 = "/airlines/%s" % airline4
    link5 = "/airlines/%s" % airline5

    avg = airlines_avg();
    avgDelayRate = avg[0]
    avgDelayTime = avg[1]
    avgCanRate = avg[2]
    avgp = avg[3]

    airdata = \
        DataPool(
           series=
            [{'options': {
               'source': airline_summary.objects.filter(airline__in=[airline1, airline2, airline3, sort[-3][0],
                                                                     sort[-4][0], sort[-5][0]])},
              'terms': [
                'airline',
                'airline_pscore']}
             ])

    airchart = Chart(
            datasource = airdata,
            series_options =
              [{'options':{
                  'type': 'bar',
                  'stacking': False},
                'terms':{
                  'airline': [
                    'airline_pscore']
                  }}],
            chart_options =
              {'title': {
                   'text': 'Worst Airlines Rankings'},
               'xAxis': {
                    'title': {
                       'text': 'Worst Airlines'}},
               'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
               'yAxis': {
                    'title': {
                       'text': 'P-Scores'},
                     'plotLines': [{
                        'color': '#FF0000',
                        'width': 4,
                        'value': avgp,
                        'zIndex':5,
                        'label':{
                            'align':'right',
                            'x':-10,
                            'rotation':0,
                            'text':'Average P-Score',
                            'style': {
                                'color': 'blue',
                                'fontWeight': 'bold'}}
            }]}
               })
    Category = "Airlines"
    statement = """* U.S. Airline Performance Scores (P-Scores) are determined by calculating the rate of delays, average length of delay,
		and rate of cancellations attributed to a given airline.  The impact of a cancellation is also given more weight (equivalent to 
		a 12 hour delay time) as compared to a delay."""
    title = "On-Time Performance Rankings - Worst Airlines"
    title2 = "On-Time Performance Summary"
    title3 = "P-Scores Comparison - Worst Airlines"
    context = {"title":title,"title2":title2,"title3":title3,"Category":Category,"a1":a1,"link1":link1,"a1delays":a1delays,"a1delaylength":a1delaylength,"a1cans":a1cans,"a1p":a1p,
               "a2":a2,"link2":link2,"a2delays":a2delays,"a2delaylength":a2delaylength,"a2cans":a2cans,"a2p":a2p,
               "a3":a3,"link3":link3,"a3delays":a3delays,"a3delaylength":a3delaylength,"a3cans":a3cans,"a3p":a3p,
               "a4":a4,"link4":link4,"a4delays":a4delays,"a4delaylength":a4delaylength,"a4cans":a3cans,"a4p":a4p,
               "a5":a5,"link5":link5,"a5delays":a5delays,"a5delaylength":a5delaylength,"a5cans":a5cans,"a5p":a5p,
               "avgDelayRate":avgDelayRate,"avgDelayTime":avgDelayTime, "avgCanRate":avgCanRate,"avgp":avgp,
               "airchart":airchart,"statement":statement}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Best_Worst.html', context)
    else:
        return render(request, 'delays/Best_Worst.html', context)

def compareAirlines(request):
    if request.method == 'POST': # If the form has been submitted...
        form = CompareAirlinesForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass

            airline1 = request.POST.get('airline1')
            airline2 = request.POST.get('airline2')        

            air1 = airlineRates(airline1)
            air2 = airlineRates(airline2)
        
            air1_title = airline_ref_table.objects.get(unique_carrier_code = airline1)
            air1_title = air1_title.unique_carrier_code + " - " +air1_title.airline_description
            air1_delays = air1[0]
            air1_delay_time = air1[1]
            air1_can = air1[2]
            air1p = str(air1[3])[:4]

            air2_title = airline_ref_table.objects.get(unique_carrier_code = airline2)
            air2_title = air2_title.unique_carrier_code + " - " +air2_title.airline_description
            air2_delays = air2[0]
            air2_delay_time = air2[1]
            air2_can = air2[2]
            air2p = str(air2[3])[:4]
            link1 = "/airlines/%s" % airline1
            link2 = "/airlines/%s" % airline2

            threshold = datetime.today().date() - timedelta(430)
            airdata = \
                DataPool(
                   series=
                    [{'options': {
                       'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline1, monthdate__gt=threshold).values('month').annotate(monthly_delays=Sum('delays'))},
                      'terms': [
                        {airline1+' Delays':'monthly_delays'},
                        {'month':'month'},
                        {'month':'month'}]},
                     {'options': {
                       'source': airlineMonthRollups.objects.filter(Q(unique_carrier_code=airline1)|Q(unique_carrier_code=airline2) , monthdate__gt=threshold).values('unique_carrier_code').annotate(total_delays=Sum('delays'),total_flights=Sum('num_flights'))},
                      'terms': [
                        {'Carrier':'unique_carrier_code'},
                        {airline1+' Total Flights':'total_flights'},
                        {airline2+' Total Flights':'total_flights'}]},
                     {'options': {
                       'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline2, monthdate__gt=threshold).values('month').annotate(monthly_delays=Sum('delays'))},
                      'terms': [
                        {airline2+' Delays':'monthly_delays'},
                        {'month_2':'month'},]}])

            airchart = Chart(
                datasource = airdata,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':1},
                    'terms':{
                      'month': [
                        airline1+' Delays']}},
                   {'options':{
                      'type': 'pie',
                      'center': [120, 50],
                      'size': '40%'},
                    'terms':{
                      'Carrier': [
                        airline1+' Total Flights',
                        airline2+' Total Flights']}},
                    {'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':2},
                    'terms':{
                      'month_2': [
                        airline2+' Delays']
                      }}
                   ],
                chart_options =
                  {'title': {
                       'text': 'Compare Airline Delays'},
                   'xAxis': {
                        'title': {
                           'text': 'Delays by Month'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = [(None, monthname, False)])

            airdata2 = \
                DataPool(
                   series=
                    [{'options': {
                       'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline1, monthdate__gt=threshold).values('month').annotate(monthly_cans=Sum('cancellations'))},
                      'terms': [
                        {airline1+' Cancellations':'monthly_cans'},
                        {'month':'month'}]},
                     {'options': {
                       'source': airlineMonthRollups.objects.filter(Q(unique_carrier_code=airline1)|Q(unique_carrier_code=airline2) , monthdate__gt=threshold).values('unique_carrier_code').annotate(total_delays=Sum('delays'),total_flights=Sum('num_flights'))},
                      'terms': [
                        {'Carrier':'unique_carrier_code'},
                        {airline1+' Total Flights':'total_flights'},
                        {airline2+' Total Flights':'total_flights'}]},
                     {'options': {
                       'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline2, monthdate__gt=threshold).values('month').annotate(monthly_cans=Sum('cancellations'))},
                      'terms': [
                        {airline2+' Cancellations':'monthly_cans'},
                        {'month_2':'month'},]}])

            airchart2 = Chart(
                datasource = airdata2,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':1},
                    'terms':{
                      'month': [
                        airline1+' Cancellations']}},
                   {'options':{
                      'type': 'pie',
                      'center': [120, 50],
                      'size': '40%'},
                    'terms':{
                      'Carrier': [
                        airline1+' Total Flights',
                        airline2+' Total Flights']}},
                    {'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':2},
                    'terms':{
                      'month_2': [
                        airline2+' Cancellations']
                      }}
                   ],
                chart_options =
                  {'title': {
                       'text': 'Compare Airline Cancellations'},
                   'xAxis': {
                        'title': {
                           'text': 'Cancellations by Month'}},
                   'credits': {'enabled':False},
                   'yAxis': {
                        'title': {
                           'text': 'Total Cancellations'},
                            'min':0}
                   },
                x_sortf_mapf_mts = [(None, monthname, False)])
            
            Category = "Airlines"
            cht_list=[airchart,airchart2]    
            title = "On-Time Performance Comparison"
            title2 = "On-Time Performance Summary"
            title3 = "Monthly Trends Comparison"
            context = {"title":title,"title2":title2,"title3":title3,"air1_title":air1_title,"air2_title":air2_title,
                               "air1_delays":air1_delays,"air2_delays":air2_delays,
                               "air1_delay_time":air1_delay_time,"air2_delay_time":air2_delay_time,
                               "air1_can":air1_can,"air2_can":air2_can, "air1p":air1p,"air2p":air2p,"link1":link1,"link2":link2,
                               'cht_list':cht_list,"Category":Category}
            return render(request, 'delays/Compare_results.html', context)
            device = get_device(request)
            if device == 'mobile':
                return render(request, 'mobile/delays/Compare_results.html', context)
            else:
                return render(request, 'delays/Compare_results.html', context)
    else:
        form = CompareAirlinesForm()

    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Airlines.html', {'form': form,})
    else:
        return render(request, 'delays/Airlines.html', {'form': form,})
    

def compareAirports(request):
    if request.method == 'POST': # If the form has been submitted...
        form = CompareAirportsForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            airport1 = request.POST.get('airport1')
            airport2 = request.POST.get('airport2')

            air1 = airportRates(airport1)
            air2 = airportRates(airport2)

            air1_title = airport_ref_table.objects.get(unique_airport_id = airport1)
            air1_title = air1_title.airport_code + " - " +air1_title.airport_name
            air1_delays = air1[0]
            air1_delay_time = air1[1]
            air1_can = air1[2]
            air1p = str(air1[3])[:4]
            
            air2_title = airport_ref_table.objects.get(unique_airport_id = airport2)
            air2_title = air2_title.airport_code + " - " +air2_title.airport_name
            air2_delays = air2[0]
            air2_delay_time = air2[1]
            air2_can = air2[2]
            air2p = str(air2[3])[:4]
            link1 = "/airports/%s" % airport1
            link2 = "/airports/%s" % airport2

            threshold = datetime.today().date() - timedelta(430)
            airdata = \
                DataPool(
                   series=
                    [{'options': {
                       'source': airportMonthRollups.objects.filter(unique_airport_id=airport1, monthdate__gt=threshold).values('month').annotate(monthly_delays=Sum('delays'))},
                      'terms': [
                        {air1_title+' Delays':'monthly_delays'},
                        {'month':'month'}]},
                     {'options': {
                       'source': airportMonthRollups.objects.filter(Q(unique_airport_id=airport1)|Q(unique_airport_id=airport2) , monthdate__gt=threshold).values('airport_code').annotate(total_delays=Sum('delays'),total_flights=Sum('num_flights'))},
                      'terms': [
                        {'Airport':'airport_code'},
                        {air1_title+' Total Flights':'total_flights'},
                        {air2_title+' Total Flights':'total_flights'}]},
                     {'options': {
                       'source': airportMonthRollups.objects.filter(unique_airport_id=airport2, monthdate__gt=threshold).values('month').annotate(monthly_delays=Sum('delays'))},
                      'terms': [
                        {air2_title+' Delays':'monthly_delays'},
                        {'month_2':'month'},]}])

            airchart = Chart(
                datasource = airdata,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':1},
                    'terms':{
                      'month': [
                        air1_title+' Delays']}},
                   {'options':{
                      'type': 'pie',
                      'center': [120, 50],
                      'size': '40%'},
                    'terms':{
                      'Airport': [
                        air1_title+' Total Flights',
                        air2_title+' Total Flights']}},
                    {'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':2},
                    'terms':{
                      'month_2': [
                        air2_title+' Delays']
                      }}
                   ],
                chart_options =
                  {'title': {
                       'text': 'Compare Airport Delays'},
                   'xAxis': {
                        'title': {
                           'text': 'Delays by Month'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = [(None, monthname, False)])

            airdata2 = \
                DataPool(
                   series=
                    [{'options': {
                       'source': airportMonthRollups.objects.filter(unique_airport_id=airport1, monthdate__gt=threshold).values('month').annotate(monthly_cans=Sum('cancellations'))},
                      'terms': [
                        {air1_title+' Cancellations':'monthly_cans'},
                        {'month':'month'}]},
                     {'options': {
                       'source': airportMonthRollups.objects.filter(Q(unique_airport_id=airport1)|Q(unique_airport_id=airport2) , monthdate__gt=threshold).values('airport_code').annotate(total_delays=Sum('delays'),total_flights=Sum('num_flights'))},
                      'terms': [
                        {'Airport':'airport_code'},
                        {air1_title+' Total Flights':'total_flights'},
                        {air2_title+' Total Flights':'total_flights'}]},
                     {'options': {
                       'source': airportMonthRollups.objects.filter(unique_airport_id=airport2, monthdate__gt=threshold).values('month').annotate(monthly_cans=Sum('cancellations'))},
                      'terms': [
                        {air2_title+' Cancellations':'monthly_cans'},
                        {'month_2':'month'},]}])

            airchart2 = Chart(
                datasource = airdata2,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':1,
                      'color':'#FF9933'},
                    'terms':{
                      'month': [
                        air1_title+' Cancellations']}},
                   {'options':{
                      'type': 'pie',
                      'center': [120, 50],
                      'size': '40%'},
                    'terms':{
                      'Airport': [
                        air1_title+' Total Flights',
                        air2_title+' Total Flights']}},
                    {'options':{
                      'type': 'line',
                      'stacking': 'False',
                      'stack':2,
                      'color':'#003399'},
                    'terms':{
                      'month_2': [
                        air2_title+' Cancellations']
                      }}
                   ],
                chart_options =
                  {'title': {
                       'text': 'Compare Airport Cancellations'},
                   'colors': ['#FF9933','#003399'],
                   'xAxis': {
                        'title': {
                           'text': 'Cancellations by Month'}},
                   'credits': {'enabled':False},
                   'yAxis': {
                        'title': {
                           'text': 'Total Cancellations'},
                            'min':0}
                   },
                x_sortf_mapf_mts = [(None, monthname, False)])
            
            Category = "Airports"
            cht_list=[airchart,airchart2]    
            title = "On-Time Performance Comparison"
            title2 = "On-Time Performance Summary"
            title3 = "Monthly Trends Comparison"
            context = {"title":title,"title2":title2,"title3":title3,"link1":link1,"link2":link2,
                       "air1_title":air1_title,"air2_title":air2_title,
                       "air1_delays":air1_delays,"air2_delays":air2_delays,
                       "air1_delay_time":air1_delay_time,"air2_delay_time":air2_delay_time,
                       "air1_can":air1_can,"air2_can":air2_can,"air1p":air1p,"air2p":air2p,
                       "cht_list":cht_list,"Category":Category}
            device = get_device(request)
            if device == 'mobile':
                return render(request, 'mobile/delays/Compare_results.html', context) 
            else:
                return render(request, 'delays/Compare_results.html', context) 
    else:
        form = CompareAirportsForm()

    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Airports.html', {'form': form,})
    else:
        return render(request, 'delays/Airports.html', {'form': form,})

def routeLookup(request):
    if request.method == 'POST': # If the form has been submitted...
        form = AirportsRouteForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            origin_airport = request.POST.get('origin_airport')
            dest_airport = request.POST.get('destination_airport')
            
            cursor = connection.cursor()
            cursor.execute('''select unique_carrier, delays/num_flights as drate, cancellations/num_flights as crate,
                                avg_delay_time from delays_airlinesroutesummary where origin_airport = %s and dest_airport = %s
                                and unique_carrier != 'CO' group by unique_carrier''',[origin_airport,dest_airport])
            f = cursor.fetchall()
            cursor.close()

            route = airportsRouteSummary.objects.get(origin_airport=origin_airport, dest_airport=dest_airport)
            drate = str(100*(float(route.delays)/float(route.num_flights)))[:4]
            dtime = route.avg_delay_time
            crate = str(100*(float(route.cancellations)/float(route.num_flights)))[:4]
            flights = []
            for e in f:
                air_name = airline_ref_table.objects.get(unique_carrier_code = e[0])
                if e[1] is None:
                    rdrate = 0
                    ravg_delay_time = 0
                else:
                    rdrate = str(e[1]*100)[:4]
                    ravg_delay_time = e[3]
                if e[2] is None:
                    rcan_rate = 0
                else:    
                    rcan_rate = str(e[2]*100)[:4]
                link = "/airlines/%s" % e[0]
                flights.append((air_name,rdrate,rcan_rate,ravg_delay_time,link))
                
            airdata = \
            DataPool(
               series=
                [{'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2012).values('month').annotate(delays=Sum('delays'))},
                    'terms':[
                        {'2012 Month':'month'},
                        {'2012 Delays':'delays'}]},
                 {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2011).values('month').annotate(delays=Sum('delays'))},
                    'terms':[
                        {'2011 Month':'month'},
                        {'2011 Delays':'delays'}]},
                 {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2010).values('month').annotate(delays=Sum('delays'))},
                    'terms':[
                        {'2010 Month':'month'},
                        {'2010 Delays':'delays'}]},
                {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2013).values('month').annotate(delays=Sum('delays'))},
                  'terms': [
                    {'2013 Month':'month'},
                    {'2013 Delays':'delays'}]}])

            airchart = Chart(
                datasource = airdata,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': False,
                      'stack':0},
                    'terms':{
                      '2012 Month': [
                        '2012 Delays'],
                    '2011 Month': [
                        '2011 Delays'],
                   '2010 Month': [
                        '2010 Delays'],
                    '2013 Month': [
                        '2013 Delays']}}],
                chart_options =
                  {'title': {
                       'text': 'On-Time Route Performance'},
                   'xAxis': {
                        'type': 'datetime',
                        'title': {
                           'text': 'Months'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Route Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = (None, monthname, False))

            airdata2 = \
            DataPool(
               series=
                [{'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2012).values('month').annotate(cancellations=Sum('cancellations'))},
                    'terms':[
                        {'2012 Month':'month'},
                        {'2012 Cancellations':'cancellations'}]},
                 {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2011).values('month').annotate(cancellations=Sum('cancellations'))},
                    'terms':[
                        {'2011 Month':'month'},
                        {'2011 Cancellations':'cancellations'}]},
                 {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2010).values('month').annotate(cancellations=Sum('cancellations'))},
                    'terms':[
                        {'2010 Month':'month'},
                        {'2010 Cancellations':'cancellations'}]},
                {'options': {
                   'source': routesRollups.objects.filter(origin_airport = origin_airport, dest_airport=dest_airport, year=2013).values('month').annotate(cancellations=Sum('cancellations'))},
                  'terms': [
                    {'2013 Month':'month'},
                    {'2013 Cancellations':'cancellations'}]}])

            airchart2 = Chart(
                datasource = airdata2,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': False,
                      'stack':0},
                    'terms':{
                      '2010 Month': [
                        '2010 Cancellations'],
                      '2011 Month': [
                        '2011 Cancellations'],
                      '2013 Month': [
                        '2013 Cancellations'],
                      '2012 Month': [
                        '2012 Cancellations']}}],
                chart_options =
                  {'title': {
                       'text': 'On-Time Route Performance'},
                   'xAxis': {
                        'type': 'datetime',
                        'title': {
                           'text': 'Months'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Route Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = (None, monthname, False))
            cht_list = [airchart,airchart2]
            origin_link = "/airports/%s" % origin_airport
            origin = airport_ref_table.objects.get(unique_airport_id = origin_airport)
            dest_link = "/airports/%s" % dest_airport
            dest = airport_ref_table.objects.get(unique_airport_id = dest_airport)
            title= "Airport Route Performance Results"
            title2 = "Overall Route On-Time Performance Summary"
            title3 = "Airline Route On-Time Performance Summary"
            title4 = "Route Monthly Trends"
            context = {"title":title,"title2":title2,"title3":title3,"title4":title4,"drate":drate,"crate":crate,"dtime":dtime,
                       "flights":flights,"cht_list":cht_list,"origin_link":origin_link,"origin":origin,"dest":dest,"dest_link":dest_link}
            
            device = get_device(request)
            if device == 'mobile':
                return render(request, 'mobile/delays/Airports_Route_results.html', context)
            else:
                return render(request, 'delays/Airports_Route_results.html', context)
            
    else:
        form = AirportsRouteForm()

    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Airports_Route.html', {'form': form,})
    else:
        return render(request, 'delays/Airports_Route.html', {'form': form,})
    
def airLookup(request):
    if request.method == 'POST': # If the form has been submitted...
        form = AirDelaysForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            airline = request.POST.get('airline')
            airport = request.POST.get('airport')
            context={}
            #Airport only 
            if airline == '' and airport != '':
                return redirect('/airports/%s' % airport)

            #Airline only
            elif airport == '' and airline != '':
                return redirect('/airlines/%s' % airline)
                
            #Search combination
            elif airport != '' and airline != '':
                airportsAvg = airports_avg()
                airportName = airport_ref_table.objects.get(unique_airport_id = airport)
                airportScores = airportRates(airport)
                airportCanRate = airportScores[2]
                airportDelayRate = airportScores[0]
                airportDelayTime = airportScores[1]
                airportPscore = str(airportScores[3])[:4]

                airlineName = airline_ref_table.objects.get(unique_carrier_code = airline)
                airlineScores = airlineRates(airline)
                airlineCanRate = airlineScores[2]
                airlineDelayRate = airlineScores[0]
                airlineDelayTime = airlineScores[1]
                airlinePscore = str(airlineScores[3])[:4]
                airport_link = "/airports/%s" % airport
                airline_link = "/airlines/%s" % airline

                cursor = connection.cursor()

                cursor.execute('''select count(id) as num from delays_airline_delays
                                    where unique_carrier = %s and origin_airport = %s''',[airline,airport])
                check = cursor.fetchone()[0]
                if check > 0:
                    cursor.execute('''select tmp1.gt/total.tot as per from (select count(airport_pscore) gt
                                    from delays_airport_summary where airport_pscore > %s) as tmp1,
                                    (select count(airport_pscore) as tot from delays_airport_summary) as total''', [airportPscore])
                    airportStanding = str(cursor.fetchone()[0]*100)[0:4]

                    cursor.execute('''select tmp1.gt/total.tot as per from (select count(airline_pscore) gt
                                    from delays_airline_summary where airline_pscore > %s) as tmp1,
                                    (select count(airline_pscore) as tot from delays_airline_summary) as total''', [airlinePscore])
                    airlineStanding = str(cursor.fetchone()[0]*100)[0:4]

                    cursor.execute('''select tabA.delays/tabB.num, tabA.length from (SELECT count(arrival_delay) as delays, avg(arrival_delay) as length
                                    FROM delays_airline_delays where unique_carrier = %s
                                    and origin_airport = %s and arrival_delay >0) as tabA,
                                    (select count(id) as num from delays_airline_delays
                                    where unique_carrier = %s and origin_airport = %s) as tabB''',[airline,airport,airline,airport] )
                    delays = cursor.fetchall()
                    if delays[0][0] is None:
                        delaysRate = 0
                        delaysAvg = 0
                    else:    
                        delaysRate = str(delays[0][0]*100)[:4]
                        delaysAvg = str(delays[0][1])[:2]

                    cursor.execute('''select tabA.cans/tabB.num from (SELECT count(canceled) as cans
                                        FROM delays_airline_delays where unique_carrier = %s
                                        and origin_airport = %s and canceled >0) as tabA,
                                        (select count(id) as num from delays_airline_delays
                                        where unique_carrier = %s and origin_airport = %s) as tabB''',[airline,airport,airline,airport] )
                    cans = cursor.fetchone()
                    if cans[0] is not None:
                        cansRate = str(cans[0]*100)[:4]
                    else:
                        cansRate = 0
                              
                    cursor.close()

                    threshold = datetime.today().date() - timedelta(430)
                    
                    airdata = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, arrival_delay__gt=1, year=2012).values('month').annotate(delays=Count('arrival_delay'))},
                          'terms': [
                            {'2012 month':'month'},
                            {'2012 Delays':'delays'}]},
                         {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, arrival_delay__gt=1, year=2010).values('month').annotate(delays=Count('arrival_delay'))},
                          'terms': [
                            {'2010 month':'month'},
                            {'2010 Delays':'delays'}]},
                         {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, arrival_delay__gt=1, year=2011).values('month').annotate(delays=Count('arrival_delay'))},
                          'terms': [
                            {'2011 month':'month'},
                            {'2011 Delays':'delays'}]},
                        {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, arrival_delay__gt=1, year=2013).values('month').annotate(delays=Count('arrival_delay'))},
                          'terms': [
                            {'2013 month':'month'},
                            {'2013 Delays':'delays'}]}])

                    airchart = Chart(
                        datasource = airdata,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False,
                              'stack':0},
                            'terms':{
                              '2012 month': [
                                '2012 Delays'],
                              '2011 month': [
                                '2011 Delays'],
                              '2010 month': [
                                '2010 Delays'],
                              '2013 month': [
                                '2013 Delays']
                              }}],
                        chart_options =
                          {'title': {
                               'text': 'On-Time Route Performance'},
                           'xAxis': {
                                'title': {
                                   'text': 'Months'}},
                           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                           'yAxis': {
                                'title': {
                                   'text': 'Total Delays'},
                                    'min':0}
                           },
                        x_sortf_mapf_mts = (None, monthname, False))

                    airdata2 = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, year=2012).values('month').annotate(cancellations=Sum('canceled'))},
                          'terms': [
                            {'2012 month':'month'},
                            {'2012 Cancellations':'cancellations'}]},
                         {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, year=2011).values('month').annotate(cancellations=Sum('canceled'))},
                          'terms': [
                            {'2011 month':'month'},
                            {'2011 Cancellations':'cancellations'}]},
                         {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, year=2010).values('month').annotate(cancellations=Sum('canceled'))},
                          'terms': [
                            {'2010 month':'month'},
                            {'2010 Cancellations':'cancellations'}]},
                        {'options': {
                           'source': airline_delays.objects.filter(origin_airport = airport, unique_carrier = airline, year=2013).values('month').annotate(cancellations=Sum('canceled'))},
                          'terms': [
                            {'2013 month':'month'},
                            {'2013 Cancellations':'cancellations'}]}])

                    airchart2 = Chart(
                        datasource = airdata2,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False,
                              'stack':0},
                            'terms':{
                              '2012 month': [
                                '2012 Cancellations'],
                              '2010 month': [
                                '2010 Cancellations'],
                              '2011 month': [
                                '2011 Cancellations'],
                              '2013 month': [
                                '2013 Cancellations']
                              }}],
                        chart_options =
                          {'title': {
                               'text': 'On-Time Route Performance'},
                           'xAxis': {
                                'title': {
                                   'text': 'Months'}},
                           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                           'yAxis': {
                                'title': {
                                   'text': 'Total Delays'},
                                    'min':0}
                           },
                        x_sortf_mapf_mts = (None, monthname, False))

                    cht_list=[airchart,airchart2]
                    meta = "%s, %s, delays, cancellations, flights, performance, US carriers, airline delays, on-time" % (airlineName, airportName)
                    Category = "Airlines"
                    title = "Lookup Results"
                    title2 = "%s Departing %s On-Time Performance" % (airlineName,airportName)
                    title3 = "%s Departing %s Monthly Trends" % (airlineName,airportName)
                    context = {"title":title,"title2":title2,"title3":title3,"airportName":airportName, "airportCanRate":airportCanRate,"airportDelayRate":airportDelayRate,
                               "airportDelayTime":airportDelayTime,"airportPscore":airportPscore,"airportStanding":airportStanding,
                               "airlineName":airlineName,"airlineCanRate":airlineCanRate,"airlineDelayRate":airlineDelayRate,
                               "airlineDelayTime":airlineDelayTime,"airlinePscore":airlinePscore, "airlineStanding":airlineStanding,
                               "delaysRate":delaysRate,"delaysAvg":delaysAvg,"cansRate":cansRate,"airport_link":airport_link,"airline_link":airline_link,
                               "cht_list":cht_list,"meta":meta}
                    device = get_device(request)
                    if device == 'mobile':
                        return render(request, 'mobile/delays/delays_results.html', context)
                    else:
                        return render(request, 'delays/delays_results.html', context)
                else:
                    title = "Lookup Results"
                    Message = "The selected Airline does not serve the selected Airport"
                    context = {"title":title,"Message":Message}
                    
                    device = get_device(request)
                    if device == 'mobile':
                        return render(request, 'mobile/delays/delays_results.html', context)
                    else:
                        return render(request, 'delays/delays_results.html', context)
                
            else:
                form = AirDelaysForm() # An unbound form
    else:
        form = AirDelaysForm() # An unbound form
    title = "Airline/Airport Lookup"
    device = get_device(request)
    if device == 'mobile':
        return render(request,  'mobile/delays/delays_search.html', {'form': form,"title":title})
    else:
        return render(request,  'delays/delays_search.html', {'form': form,"title":title})

def flightLookup(request):    
    if request.method == 'POST': # If the form has been submitted...
        form = FlightLookupForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            airline = request.POST.get('airline')
            flight_number = request.POST.get('flight_number')

            r = flightsRollups.objects.filter(unique_carrier = airline, flight=flight_number).get()
            drate = str((float(r.delays)/float(r.num_flights))*100)[:4]
            crate = str((float(r.cancellations)/float(r.num_flights))*100)[:4]
            dtime = r.avg_delay_time

            cursor = connection.cursor()
            cursor.execute('''SELECT origin_airport, dest_airport from delays_airline_delays
                            where unique_carrier = %s and flight_number = %s
                            group by origin_airport, dest_airport''',[airline,flight_number])
            routes = cursor.fetchall()
            cursor.close()
            
            air_name = airline_ref_table.objects.filter(unique_carrier_code = airline)
            flights = []
            for e in routes:
                origin = (airport_ref_table.objects.filter(unique_airport_id = e[0]))[0]
                dest = (airport_ref_table.objects.filter(unique_airport_id = e[1]))[0]
                origin_link = "/airports/%s" % e[0]
                dest_link = "/airports/%s" %e[1]
                flights.append((origin,dest,origin_link,dest_link))

            airdata = \
            DataPool(
               series=
                [{'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, arrival_delay__gt=1, year=2012).values('month').annotate(delays=Count('arrival_delay'))},
                  'terms': [
                    {'2012 month':'month'},
                    {'2012 Delays':'delays'}]},
                 {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, arrival_delay__gt=1, year=2010).values('month').annotate(delays=Count('arrival_delay'))},
                  'terms': [
                    {'2010 month':'month'},
                    {'2010 Delays':'delays'}]},
                 {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, arrival_delay__gt=1, year=2011).values('month').annotate(delays=Count('arrival_delay'))},
                  'terms': [
                    {'2011 month':'month'},
                    {'2011 Delays':'delays'}]},
                {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, arrival_delay__gt=1, year=2013).values('month').annotate(delays=Count('arrival_delay'))},
                  'terms': [
                    {'2013 month':'month'},
                    {'2013 Delays':'delays'}]}])

            airchart = Chart(
                datasource = airdata,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': False,
                      'stack':0},
                    'terms':{
                      '2012 month': [
                        '2012 Delays'],
                      '2011 month': [
                        '2011 Delays'],
                      '2010 month': [
                        '2010 Delays'],
                      '2013 month': [
                        '2013 Delays']}}],
                chart_options =
                  {'title': {
                       'text': 'On-Time Flight Performance'},
                   'xAxis': {
                        'title': {
                           'text': 'Months'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = (None, monthname, False))

            airdata2 = \
            DataPool(
               series=
                [{'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, year=2012).values('month').annotate(cancellations=Sum('canceled'))},
                  'terms': [
                    {'2012 month':'month'},
                    {'2012 Cancellations':'cancellations'}]},
                 {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, year=2011).values('month').annotate(cancellations=Sum('canceled'))},
                  'terms': [
                    {'2011 month':'month'},
                    {'2011 Cancellations':'cancellations'}]},
                 {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, year=2010).values('month').annotate(cancellations=Sum('canceled'))},
                  'terms': [
                    {'2010 month':'month'},
                    {'2010 Cancellations':'cancellations'}]},
                {'options': {
                   'source': airline_delays.objects.filter(unique_carrier= airline,flight_number = flight_number, year=2013).values('month').annotate(cancellations=Sum('canceled'))},
                  'terms': [
                    {'2013 month':'month'},
                    {'2013 Cancellations':'cancellations'}]}])

            airchart2 = Chart(
                datasource = airdata2,
                series_options =
                  [{'options':{
                      'type': 'line',
                      'stacking': False,
                      'stack':0},
                    'terms':{
                      '2012 month': [
                        '2012 Cancellations'],
                      '2010 month': [
                        '2010 Cancellations'],
                      '2011 month': [
                        '2011 Cancellations'],
                      '2013 month': [
                        '2013 Cancellations']}}],
                chart_options =
                  {'title': {
                       'text': 'On-Time Flight Performance'},
                   'xAxis': {
                        'title': {
                           'text': 'Months'}},
                   'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
                   'yAxis': {
                        'title': {
                           'text': 'Total Delays'},
                            'min':0}
                   },
                x_sortf_mapf_mts = (None, monthname, False))

            cht_list=[airchart,airchart2] 
            airline_link = "/airlines/%s" % airline    
            title= "Flight On-Time Performance Results"
            title2 = "Flight On-Time Performance" 
            title3 = "Flight On-Time Performance Monthly Trends"
            context = {"title":title,"title2":title2,"title3":title3,
                       "airline":airline,"air_name":air_name,"flight":flight_number,"drate":drate,"crate":crate,"dtime":dtime,
                       "flights":flights,"airline_link":airline_link,"cht_list":cht_list}
            
            device = get_device(request)
            if device == 'mobile':
                return render(request, 'mobile/delays/Flight_Lookup_results.html', context)
            else:
                return render(request, 'delays/Flight_Lookup_results.html', context)
    else:
        form = FlightLookupForm()

    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/Flight_Lookup.html', {'form': form,})
    else:
        return render(request, 'delays/Flight_Lookup.html', {'form': form,})
    

def airlineLookup(request, airline):
    airlinesAvg = airlines_avg()
    airlineName = airline_ref_table.objects.get(unique_carrier_code = airline)
    airlineScores = airlineRates(airline)
    airlineCanRate = airlineScores[2]
    airlineDelayRate = airlineScores[0]
    airlineDelayTime = airlineScores[1]
    airlinePscore = str(airlineScores[3])[:4]

    cursor = connection.cursor()
    cursor.execute('''select tmp1.gt/total.tot as per from (select count(airline_pscore) gt
                        from delays_airline_summary where airline_pscore > %s) as tmp1,
                        (select count(airline_pscore) as tot from delays_airline_summary) as total''', [airlinePscore])
    airlineStanding = str(cursor.fetchone()[0]*100)[0:4]
    cursor.close()

    info = airlineInfobox.objects.get(airline = airline)
    infobox = []
    infobox.append(info.headquarters)
    if info.frequent_flyer:
        infobox.append(info.frequent_flyer)
    else:
        infobox.append("-")

    if info.airport_lounge:
        infobox.append(info.airport_lounge)
    else:
        infobox.append("-")

    infobox.append(info.website)

    ahubs = []
    if info.hubs:
        s = info.hubs.split(',')
        for e in s:
            name = airport_ref_table.objects.get(unique_airport_id = e)
            link = "/airports/%s" % e
            ahubs.append([name, link])
    else:
        ahubs.append("")

    airdata1 = \
    DataPool(
       series=
        [{'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2012)},
          'terms': [
            {'unique_carrier_code'},
            {'2012 month':'month'},
            {'2012 Delays':'delays'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2011)},
          'terms': [
            {'unique_carrier_code'},
            {'2011 month':'month'},
            {'2011 Delays':'delays'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2010)},
          'terms': [
            {'unique_carrier_code'},
            {'2010 month':'month'},
            {'2010 Delays':'delays'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2013)},
          'terms': [
            {'unique_carrier_code'},
            {'2013 month':'month'},
            {'2013 Delays':'delays'}]}])
    
    airchart1 = Chart(
        datasource = airdata1,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{ 
              '2012 month': [
                '2012 Delays'],
              '2010 month': [
                '2010 Delays'],
              '2011 month': [
                '2011 Delays'],
              '2013 month': [
                '2013 Delays']
              }}],
        chart_options =
          {'title': {
               'text': str(airlineName)+' Delays Monthly Trends'},
           'xAxis': {
                'title': {
                   'text': 'Delays by Month'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Delays'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    airdata2 = \
    DataPool(
       series=
        [{'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2012)},
          'terms': [
            {'unique_carrier_code'},
            {'2012 month':'month'},
            {'2012 Cancellations':'cancellations'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2011)},
          'terms': [
            {'unique_carrier_code'},
            {'2011 month':'month'},
            {'2011 Cancellations':'cancellations'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2010)},
          'terms': [
            {'unique_carrier_code'},
            {'2010 month':'month'},
            {'2010 Cancellations':'cancellations'}]},
         {'options': {
           'source': airlineMonthRollups.objects.filter(unique_carrier_code=airline, year = 2013)},
          'terms': [
            {'unique_carrier_code'},
            {'2013 month':'month'},
            {'2013 Cancellations':'cancellations'}]}])
    
    airchart2 = Chart(
        datasource = airdata2,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{ 
              '2012 month': [
                '2012 Cancellations'],
              '2010 month': [
                '2010 Cancellations'],
              '2011 month': [
                '2011 Cancellations'],
              '2013 month': [
                '2013 Cancellations']
              }}],
        chart_options =
          {'title': {
               'text': str(airlineName)+' Cancellations Monthly Trends'},
           'xAxis': {
                'title': {
                   'text': 'Cancellations by Month'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Cancellations'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))
    logo = "images/logos/%s.png" % airline

    meta = "%s, delays, cancellations, flights, performance, US carriers, airline delays, on-time" % airlineName
    title = airlineName
    cht_list=[airchart1, airchart2]
    context = {"title":title,"airlineName":airlineName,"airlineCanRate":airlineCanRate,"airlineDelayRate":airlineDelayRate,
               "airlineDelayTime":airlineDelayTime,"airlinePscore":airlinePscore,"airlineStanding":airlineStanding,
               "cht_list":cht_list, "logo":logo, "infobox":infobox, "ahubs":ahubs,"meta":meta}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/delays_results.html', context)
    else:
        return render(request, 'delays/delays_results.html', context)

def airportLookup(request, airport):
    airportsAvg = airports_avg()
    airportName = airport_ref_table.objects.get(unique_airport_id = airport)
    airportScores = airportRates(airport)
    airportCanRate = airportScores[2]
    airportDelayRate = airportScores[0]
    airportDelayTime = airportScores[1]
    airportPscore = str(airportScores[3])[:4]

    cursor = connection.cursor()
    cursor.execute('''select tmp1.gt/total.tot as per from (select count(airport_pscore) gt
                        from delays_airport_summary where airport_pscore > %s) as tmp1,
                        (select count(airport_pscore) as tot from delays_airport_summary) as total''', [airportPscore])
    airportStanding = str(cursor.fetchone()[0]*100)[0:4]
    cursor.close()

    info = airportInfobox.objects.get(airport_code = airportName.airport_code)
    infobox = []
    city = info.city+' '+info.state
    infobox.append(city)
    infobox.append(info.elevation)
    infobox.append(info.time_zone)
    keyword = airportName.airport_name
    keyword = urllib2.quote(keyword)
    dlat = info.latitude
    dlong = info.longitude

    if cache.get(airport) is None:
        airport_cache = []
        place_search = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%s,%s&keyword=%s&radius=500&types=airport&sensor=false&key=AIzaSyAiyOYbkVSBwMs9dRnQQcMMIvhbHz__WSM" % (dlat,dlong,keyword)
        response = urllib2.urlopen(place_search)
        data = json.load(response)
        if data["status"] != "ZERO_RESULTS":
            lat = data["results"][0]["geometry"]["location"]["lat"]
            longi = data["results"][0]["geometry"]["location"]["lng"]
            place_ref = data["results"][0]["reference"]
            
            static_map = "https://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=14&maptype=hybrid&size=300x300&sensor=false" % (lat,longi)
            #static_map = "http://open.mapquestapi.com/staticmap/v4/getmap?key=Fmjtd%%7Cluub2d6z2u%%2Ca5%%3Do5-9u7w9r&center=%s,%s&zoom=13&size=300,300&type=hyb&imagetype=jpeg&scalebar=false&pois=" % (lat,longi)
            place_details = "https://maps.googleapis.com/maps/api/place/details/json?reference=%s&sensor=false&key=AIzaSyAiyOYbkVSBwMs9dRnQQcMMIvhbHz__WSM" % place_ref
            response2 = urllib2.urlopen(place_details)
            data2 = json.load(response2)
            airport_cache.append(lat)
            airport_cache.append(longi)
            airport_cache.append(place_ref)
            
            if data2:
                website = data2["result"]["website"]
                infobox.append(website)
                airport_cache.append(website)
            cache.set(airport, airport_cache)
        else:
            static_map = "https://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=14&maptype=hybrid&size=300x300&sensor=false" % (dlat,dlong)
            airport_cache.append(dlat)
            airport_cache.append(dlong)
            airport_cache.append('')
            airport_cache.append('')
            cache.set(airport,airport_cache)
            
    else:
        airport_cache = cache.get(airport)
        lat = airport_cache[0]
        longi = airport_cache[1]
        place_ref = airport_cache[2]
        website = airport_cache[3]
        infobox.append(website)
        static_map = "https://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=14&maptype=hybrid&size=300x300&sensor=false" % (lat,longi)
        
    airdata = \
    DataPool(
       series=
        [{'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year=2012)},
          'terms': [
            {'unique_airport_id'},
            {'2012 month':'month'},
            {'2012 Delays':'delays'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year=2011)},
          'terms': [
            {'unique_airport_id'},
            {'2011 month':'month'},
            {'2011 Delays':'delays'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year=2010)},
          'terms': [
            {'unique_airport_id'},
            {'2010 month':'month'},
            {'2010 Delays':'delays'}]},
           {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year=2013)},
          'terms': [
            {'unique_airport_id'},
            {'2013 month':'month'},
            {'2013 Delays':'delays'}]} 
          ])
    
    airchart = Chart(
        datasource = airdata,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{ 
              '2012 month': [
                '2012 Delays'],
              '2011 month': [
                '2011 Delays'],
              '2010 month': [
                '2010 Delays'],
              '2013 month': [
                '2013 Delays']
              }}],
        chart_options =
          {'title': {
               'text': str(airportName)+' Delays Monthly Trends'},
           'xAxis': {
                'title': {
                   'text': 'Delays by Month'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Delays'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    airdata2 = \
    DataPool(
       series=
        [{'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year = 2012)},
          'terms': [
            {'unique_airport_id'},
            {'2012 month':'month'},
            {'2012 Cancellations':'cancellations'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year = 2010)},
          'terms': [
            {'unique_airport_id'},
            {'2010 month':'month'},
            {'2010 Cancellations':'cancellations'}]},
         {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year = 2011)},
          'terms': [
            {'unique_airport_id'},
            {'2011 month':'month'},
            {'2011 Cancellations':'cancellations'}]},
            {'options': {
           'source': airportMonthRollups.objects.filter(unique_airport_id=airport, year = 2013)},
          'terms': [
            {'unique_airport_id'},
            {'2013 month':'month'},
            {'2013 Cancellations':'cancellations'}]}
         ])
    
    airchart2 = Chart(
        datasource = airdata2,
        series_options =
          [{'options':{
              'type': 'line',
              'stacking': False,
              'stack':0},
            'terms':{ 
              '2010 month': [
                '2010 Cancellations'],
              '2012 month': [
                '2012 Cancellations'],
              '2011 month': [
                '2011 Cancellations'],
              '2013 month': [
                '2013 Cancellations']
              }}],
        chart_options =
          {'title': {
               'text': str(airportName)+' Cancellations Monthly Trends'},
           'xAxis': {
                'title': {
                   'text': 'Cancellations by Month'}},
           'credits': {'text': 'Select Legend Category to Remove/Add Series to Chart'},
           'yAxis': {
                'title': {
                   'text': 'Total Cancellations'},
                    'min':0}
           },
        x_sortf_mapf_mts = (None, monthname, False))

    title = airportName
    meta = "%s, delays, cancellations, flights, performance, airport delays, on-time" % airportName
    cht_list = [airchart,airchart2]
    context = {"title":title,"airportName":airportName, "airportCanRate":airportCanRate,"airportDelayRate":airportDelayRate,
               "airportDelayTime":airportDelayTime,"airportPscore":airportPscore,"airportStanding":airportStanding,
               "cht_list":cht_list, "infobox":infobox, "static_map":static_map,"meta":meta}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/delays_results.html', context)
    else:
        return render(request, 'delays/delays_results.html', context)
    
def trends(request):
    t = airlineTrends.objects.all()
    adata = []
    #adata = [['Airline','3 Month Airline Delays','3 Month Airline Cancellations','','3 Month Airline P-Score'],
     #        ['AA',67,33,1,33]]
    adata.append(['Airline','3 Month Airline Cancellations Rate (% cancelled)','3 Month Airline Delays Rate (% delayed)','','3 Month Airline P-Score'])
    for e in t:
        a = [str(e.airline),e.airline_crate,e.airline_drate,e.pscore,e.pscore]
        adata.append(a)
    l = airline_ref_table.objects.all()
    legend = []
    for e in l:
        name = e.unique_carrier_code+"-"+e.airline_description
        link = "/airlines/%s" % e.unique_carrier_code
        legend.append([name, link])
    title= "Airline Trends Last 3 Months"
    context = {"title":title,"adata":adata,"legend":legend}
    device = get_device(request)
    if device == 'mobile':
        return render(request, 'mobile/delays/trends.html', context)
    else:
        return render(request, 'delays/trends.html', context)


def lookups(request):
    if request.method == 'POST': # If the form has been submitted...
        if 'air_lookup' in request.POST:
            form = AirDelaysForm(request.POST) # A form bound to the POST data
            #Airport only
            if airline == '' and airport != '':
                return redirect('/airports/%s' % airport)
            #Airline only
            elif airport == '' and airline != '':
                return redirect('/airlines/%s' % airline)
            #Search combination
            elif airport != '' and airline != '':
                return airLookup(request)        
        elif 'routes_lookup' in request.POST:
            form = AirportsRouteForm(request.POST)
            return routeLookup(request)
        elif 'flights_lookup' in request.POST:
            form = FlightLookupForm(request.POST)
            return flightLookup(request)

    else:
        Form2 = AirportsRouteForm()
        Form1 = AirDelaysForm()
        Form3 = FlightLookupForm()
    
        context = {"Form3":Form3,"Form2":Form2,"Form1":Form1}
        device = get_device(request)
        if device == 'mobile':
            return render(request, 'mobile/delays/lookups.html', context)
        else:
            return render(request, 'delays/lookups.html', context)
